# ros_example

[![build_status](https://gitlab.com/uwt-rover/ros_example/badges/master/pipeline.svg)](https://gitlab.com/uwt-rover/ros_example)

## Prerequisites
You will need to have already installed ROS Melodic on your machine. Instructions
for install ROS on Ubuntu 18.04 can be found at [http://wiki.ros.org/melodic/Installation/Ubuntu](http://wiki.ros.org/melodic/Installation/Ubuntu)

## Build
If you already have a catkin workspace folder with a `src` folder inside, clone this
repo into your `src` folder and run `catkin_make`. Otherwise run

``` shell
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
git clone https://gitlab.com/uwt-rover/ros_example
cd ~/catkin_ws
catkin_make
```

`catkin_make` will create a file you need to source so your shell and ROS
know where to find the newly build package.

If you are using `zsh`, run
``` shell
source ~/catkin_ws/devel/setup.zsh
```

For all other shells, run
``` shell
source ~/catkin_ws/devel/setup.bash
```

## Run
First start `roscore` and in a separate terminal type `rosrun ros_example` and hit TAB for completion of runnable programs/scrips. Each program has a version in Python and C++.

## Contents

### Layout
#### msg
Contains message definition files for a package.

#### scripts
Contains custom ROS programs written in Python.

#### src
Contains custom ROS programs written in C++.

#### srv
Contains service definition files.

### Files
#### CMakeLists.txt
The CMake file defines what ROS packages this package depends on and what C++ files to build
and what libraries to link with.

#### package.xml
This file defines the package name, version, description, maintainer, license, and dependencies for build, build export, and execution.
